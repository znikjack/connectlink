<main id="main">
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

  <div class="container">
    <div class="row gy-4">
      <div class="col-lg-6 order-1 order-lg-1 hero-img">
        <img src="<?php echo base_url('assets/front/img/homepage/hero-img-1.png'); ?>" class="img-fluid animated" alt="">
      </div>
      <div class="col-lg-6 order-2 order-lg-2 d-flex flex-column justify-content-center">
        <h1>Make your Digital Card</h1>
        <h2>We are team of talented designers making websites with Bootstrap</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Explore</a>
        </div>
      </div>
     
    </div>
  </div>

</section><!-- End Hero -->

 <!-- ======= Feature Section ======= -->
    <section id="feature" class="feature">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Features</h2>
        </div>

          <div class="container">
            <div class="row gy-4">
              <div class="col-lg-6 order-2 order-lg-2 hero-img">
                <img src="<?php echo base_url('assets/front/img/homepage/Profile-Analytics.png'); ?>" class="img-fluid animated" alt="">
              </div>
              <div class="col-lg-6 order-1 order-lg-1 d-flex flex-column justify-content-center">
                <h2 class="title">Profile Analytics</h1>
                <p>Lorem ipsum is simply dummy text of the priting and typesetting industry. Lorem ipsum has been</p>
              </div>
            </div>


          <div class="row gy-4">
            <div class="col-lg-6 order-1 order-lg-1 hero-img">
              <img src="<?php echo base_url('assets/front/img/homepage/Connect.png'); ?>" class="img-fluid animated" alt="">
            </div>
            <div class="col-lg-6 order-2 order-lg-2 d-flex flex-column justify-content-center">
              <h2 class="title">Connect with near Users</h2>
              <p>Lorem ipsum is simply dummy text of the priting and typesetting industry. Lorem ipsum has been</p>
            </div>
          </div>
          </div>
      </div>
    </section><!-- End Feature Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Plans</h2>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-4 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <h4 class="title text-center">Basic Plan</h4>
              <h2 class="text-center">0.0$</h2>
              <!-- <p class="description"></p> -->
              <ul>
                <li>Create ConnectLink Profile</li>
                <li>Add Links, websites and Info</li>
                <li>Activate Connect Products</li>
                <li>See last 3 connection tab</li>
                <li>Profile, Connection, Business Analytics</li>
                <li>Add Social Logo, Custom icon and title for social Links</li>
                <li>Embed Videos</li>
                <li>Job Protal</li>
                <li>See Entire Connection Tab</li>
                <li>App Support Team</li>
              </ul>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 align-items-stretch text-left"  data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bx-file"></i></div> -->
              <h4 class="title text-center">Pro Plan</h4>
              <h2 class="text-center">1.99$</h2>
              <ul>
                <li>Create Event & Topic</li>
                <li>Add Multiple Accounts</li>
                <li>CRM Integrations</li>
                <li>Chat Between Connection</li>
                <li>Export Contact Connection</li>
                <li>Invite freinds and get 2 Connelink products</li>
                <li>Referral Friends </li>
                <li>Tap to Connect</li>
                <li>See Mutuak Friends </li>
                <li>Post Store about your businees part</li>
                <li>Get Job Refrence & Development Team</li>
              </ul>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bx-tachometer"></i></div> -->
              <h4 class="title text-center">Enterprise Plan</h4>
              <h2 class="text-center">3.99$</h2>
              <ul>
                <li>Add More Team Member on Enterprise Account</li>
                <li>Manage Social List by Lock/Unlock Option</li>
                <li>Add Company/Enterprise Details and there social List </li>
                <li>Advance Business Analytics</li>
                <li>Make NFC Reader Attendance with Member phone</li>
                <li>+ Pro Features</li>
              </ul>
            </div>
          </div>


        </div>

      </div>
    </section><!-- End Services Section -->


    
    <!-- ======= Social Section ======= -->
    <section id="social" class="social">
      <div class="section-title text-ceter">
          <h2>What you want to share ?</h2>
        </div>
        <div class="row">
          <img src="<?php echo base_url('assets/front/img/homepage/social-connets.png'); ?>">
        </div>
    </section><!-- End Social Section -->

  
    <!-- ======= Counter Section ======= -->

    <section id="counter" class="counter">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-md-3 col-lg-3 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <h4 class="title">24k</h4>
              <h4>Total  Users</h4>
              <!-- <p class="description"></p> -->
            </div>
          </div>
          <div class="col-md-3 col-lg-3 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <h4 class="title">890k</h4>
              <h4>Total Interactions</h4>
              <!-- <p class="description"></p> -->
            </div>
          </div>
          <div class="col-md-3 col-lg-3 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <h4 class="title">37k</h4>
              <h4>Total Download</h4>
              <!-- <p class="description"></p> -->
            </div>
          </div>
          <div class="col-md-3 col-lg-3 align-items-stretch text-left" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <h4 class="title">12k</h4>
              <h4>Premiuim Users</h4>
              <!-- <p class="description"></p> -->
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Counter Section -->


    <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact Us</h2>
        </div>

        <div class="row">
          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
                </div>
                <div class="form-group col-md-6 mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="10" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-end"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Us Section -->