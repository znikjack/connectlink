<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>ConnectLink</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="<?php echo base_url('assets/img/favicon.png'); ?>" rel="icon">
        <link href="<?php echo base_url('assets/img/apple-touch-icon.png'); ?>" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="<?php echo base_url("assets/front/vendor/aos/aos.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/vendor/bootstrap/css/bootstrap.min.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/vendor/bootstrap-icons/bootstrap-icons.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/vendor/boxicons/css/boxicons.min.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/vendor/glightbox/css/glightbox.min.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/vendor/swiper/swiper-bundle.min.css") ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/front/css/style.css") ?>" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

        <!-- =======================================================
        * Template Name: Ninestars - v4.3.0
        * Template URL: https://bootstrapmade.com/ninestars-free-bootstrap-3-theme-for-creative/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
    </head>
    <body>
        <!-- ======= Header ======= -->
        <header id="header" class="d-flex align-items-center">
            <div class="container d-flex align-items-center justify-content-between">
                <div class="logo">
                    <h1 class="text-light"><a href="#"><span>ConnetLink</span></a></h1>
                </div>
                <nav id="navbar" class="navbar">
                    <ul>
                        <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                        <li><a class="nav-link scrollto" href="#about">About Us</a></li>
                        <li><a class="nav-link scrollto" href="#services">Services</a></li>
                        <!-- <li><a class="nav-link scrollto" href="#portfolio">Portfolio</a></li> -->
                        <li><a class="nav-link scrollto" href="#team">Pricing</a></li>

                    </ul>
                    <i class="bi bi-list mobile-nav-toggle"></i>
                </nav><!-- .navbar -->
            </div>
        </header><!-- End Header -->