<style>
    .error {color: #ff00008;}
</style>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><?php echo $pagetitle; ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo base_url('admin/dashboard') ?>"> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/user') ?>">Users</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">User Profile</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">
                    <!--profile cover start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover-profile">
                                <div class="profile-bg-img">
                                    <img class="profile-bg-img img-fluid" src="<?php echo base_url('assets/images/user-profile/bg-img1.jpg'); ?>" alt="bg-img">
                                    <div class="card-block user-info">
                                        <div class="col-md-12">
                                            <div class="media-left">
                                                <a href="#" class="profile-image">
                                                    <img class="user-img img-radius" src="<?php echo base_url("assets/images/avatar-7.jpg") ?>" alt="user-img">
                                                </a>
                                            </div>
                                            <div class="media-body row">
                                                <div class="col-lg-12">
                                                    <div class="user-title">
                                                        <h2>Josephin Villa</h2>
                                                        <span class="text-white">Web designer</span>
                                                    </div>
                                                </div>
<!--                                                <div>
                                                    <div class="pull-right cover-btn">
                                                        <button type="button" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-plus"></i> Follow</button>
                                                        <button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message</button>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--profile cover end-->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- tab header start -->
                            <div class="tab-header card">
                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                                        <div class="slide"></div>
                                    </li>
<!--                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Social List</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#contacts" role="tab">Interaction</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#review" role="tab">Analysis</a>
                                        <div class="slide"></div>
                                    </li>-->
                                </ul>
                            </div>
                            <!-- tab header end -->
                            <!-- tab content start -->
                            <div class="tab-content">
                                <!-- tab panel personal start -->
                                <div class="tab-pane active" id="personal" role="tabpanel">
                                    <!-- personal card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">About Me</h5>
                                        </div>
                                        <form action="<?php echo base_url('admin/user/form'); ?>" method="POST" enctype="multipart/form-data" id="user_register">
                                        <input type="hidden" id="base_url" value="<?php echo base_url('admin'); ?>" class="base_url">
                                        <div class="card-block">
                                            <div class="view-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="general-info">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                                                        <input type="text" class="form-control" name="FirstName" placeholder="First Name">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-email"></i></span>
                                                                                        <input type="text" class="form-control" name="EmailAddress" placeholder="Email Address">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
                                                                                        <input type="text" class="form-control" name="MobileNumber" placeholder="Mobile Number">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-radio">
                                                                                        <div class="group-add-on">
                                                                                            <div class="radio radiofill radio-inline">
                                                                                                <label>
                                                                                                    <input type="radio" name="gender" value="Male" checked=""><i class="helper"></i> Male
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill radio-inline">
                                                                                                <label>
                                                                                                    <input type="radio" name="gender" value="Female"><i class="helper"></i> Female
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">User Image</span>
                                                                                        <input type="file" class="form-control" name="ProfileURL">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">Background Image</span>
                                                                                        <input type="file" class="form-control" name="BackgroundURL">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                                                        <input type="text" class="form-control" name="LastName" placeholder="Last Name">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
                                                                                        <input type="text" class="form-control" name="address" placeholder="Address">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><i class="icofont icofont-key"></i></span>
                                                                                        <input type="password" class="form-control" placeholder="Password" name="Password">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <input id="dropper-default" class="form-control" type="text" name="bod" placeholder="Select Your Birth Date">
                                                                                </td>
                                                                            </tr>
                                                                              <tr>
                                                                                <td>
                                                                                    <div class="input-group">
                                                                                        <textarea rows="5" cols="5" class="form-control" name="Bio" placeholder="BIO"></textarea>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of general info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                 <div class="text-center">
                                                    <input type="submit" name="submit" value="Save" class="btn btn-primary waves-effect waves-light m-r-20">
                                                    <a href="<?php echo base_url('admin/user/add'); ?>" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of view-info -->
                                            <!-- end of edit-info -->
                                        </div>
                                        </form>
                                        <!-- end of card-block -->
                                    </div>
                                    <!-- personal card end-->
                                </div>
                                <!-- tab pane personal end -->
                                <!-- tab pane info start -->
                                <div class="tab-pane" id="binfo" role="tabpanel">
                                    <!-- info card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">User Services</h5>
                                        </div>
                                        <div class="card-block">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="card b-l-success business-info services m-b-20">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">Shivani Hero</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card b-l-danger business-info services">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">Dress and Sarees</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card b-l-info business-info services">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">Shivani Auto Port</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card b-l-warning business-info services">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">Hair stylist</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card b-l-danger business-info services">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">BMW India</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card b-l-success business-info services">
                                                        <div class="card-header">
                                                            <div class="service-header">
                                                                <a href="#">
                                                                    <h5 class="card-header-text">Shivani Hero</h5>
                                                                </a>
                                                            </div>
                                                            <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i> Edit</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i> Delete</a>
                                                                <a class="dropdown-item" href="#!"><i class="icofont icofont-eye-alt"></i> View</a>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                                                </div>
                                                                <!-- end of col-sm-8 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of card-block -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="card-header-text">Profit</h5>
                                                </div>
                                                <div class="card-block">
                                                    <div id="main" style="height:300px;width: 100%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- info card end -->
                                </div>
                                <!-- tab pane info end -->
                                <!-- tab pane contact start -->
                                <div class="tab-pane" id="contacts" role="tabpanel">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4 class="card-title">Contacts<span class="f-15"> (100)</span></h4>
                                                </div>
                                                <div class="card-block">
                                                    <div class="connection-list">
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-1.jpg'); ?>" alt="f-1" data-toggle="tooltip" data-placement="top" data-original-title="Airi Satou">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-2.jpg'); ?>" alt="f-2" data-toggle="tooltip" data-placement="top" data-original-title="Angelica Ramos">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-3.jpg'); ?>" alt="f-3" data-toggle="tooltip" data-placement="top" data-original-title="Ashton Cox">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-4.jpg'); ?>" alt="f-4" data-toggle="tooltip" data-placement="top" data-original-title="Cara Stevens">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-5.jpg'); ?>" alt="f-5" data-toggle="tooltip" data-placement="top" data-original-title="Garrett Winters">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- user contact card left side end -->
                                        </div>
                                    </div>
                                </div>
                                <!-- tab pane contact end -->
                                <div class="tab-pane" id="review" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Review</h5>
                                        </div>
                                        <div class="card-block">
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left">
                                                        <a href="#">
                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-1.jpg'); ?>" alt="Generic placeholder image">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="media-heading">Sortino media<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                        <div class="stars-example-css review-star">
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                        </div>
                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                        <div class="m-b-25">
                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                        </div>
                                                        <hr>
                                                        <!-- Nested media object -->
                                                        <div class="media mt-2">
                                                            <a class="media-left" href="#">
                                                                <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-2.jpg'); ?>" alt="Generic placeholder image">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                                <!-- Nested media object -->
                                                                <div class="media mt-2">
                                                                    <div class="media-left">
                                                                        <a href="#">
                                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-3.jpg'); ?>" alt="Generic placeholder image">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                        <div class="stars-example-css review-star">
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                        </div>
                                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                        <div class="m-b-25">
                                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Nested media object -->
                                                        <div class="media mt-2">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-1.jpg'); ?>" alt="Generic placeholder image">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Cedric Kelly<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="media mt-2">
                                                            <a class="media-left" href="#">
                                                                <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-4.jpg'); ?>" alt="Generic placeholder image">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                                <!-- Nested media object -->
                                                                <div class="media mt-2">
                                                                    <div class="media-left">
                                                                        <a href="#">
                                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-3.jpg'); ?>" alt="Generic placeholder image">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                        <div class="stars-example-css review-star">
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                        </div>
                                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                        <div class="m-b-25">
                                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="media mt-2">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-2.jpg'); ?>" alt="Generic placeholder image">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Mark Doe<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Right addon">
                                                <span class="input-group-addon"><i class="icofont icofont-send-mail"></i></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- tab content end -->
                        </div>
                    </div>
                </div>
                <!-- Page-body end -->
            </div>
        </div>
    </div>
</div>