<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><?= $pagetitle ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo base_url('admin/dashboard'); ?>"> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/social'); ?>">Social</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Inputs Validation start -->
                            <div class="card">
                               <div class="card-block">
                                    <form method="post" action="<?php echo base_url('admin/social/form/'.$CategoryData->id) ?>" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Category</label>
                                            <div class="col-sm-6">
                                                <select id="hello-single" class="form-control" name="Social_Header_ID">
                                                    <?php if (!empty($social_category)) { ?>
                                                        <option value="cheese">Please Select Category</option>
                                                        <?php foreach ($social_category as $row) { ?>
                                                            <option value="<?= $row->id ?>" <?php if($social->id == $row->id) {echo 'selected';} ?>><?= $row->Social_Header_Name ?></option>
                                                        <?php }
                                                    } ?>
                                                </select>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Title </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_Title" id="name" placeholder="Enter Social Title" value="<?= $social->Social_Title ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Field Name</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_Name" id="name" placeholder="Enter Social Field Name" value="<?= $social->Social_Name ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Place Holder</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_PlaceHolder" id="name" placeholder="Enter Social Place Holder" value="<?= $social->Social_PlaceHolder ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Input Type</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_Type" id="name" placeholder="Enter Social Input Type" value="<?= $social->Social_Type ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Field Class</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_Class" id="name" placeholder="Enter Social Field Class" value="<?= $social->Social_Class ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Field Type</label>
                                            <div class="col-sm-6">
                                                <select id="hello-single" class="form-control" name="Social_Input_Type">
                                                    <option value="text" <?php if($social->Social_Input_Type == 'text') {echo 'selected';} ?>>Text</option>
                                                    <option value="radio" <?php if($social->Social_Input_Type == 'radio') {echo 'selected';} ?>>Radio</option>
                                                    <option value="checkbox" <?php if($social->Social_Input_Type == 'checkbox') {echo 'selected';} ?>>Checkbox</option>
                                                    <option value="number"<?php if($social->Social_Input_Type == 'number') {echo 'selected';} ?>>Number</option>
                                                    <option value="password" <?php if($social->Social_Input_Type == 'password') {echo 'selected';} ?>>Password</option>
                                                    <option value="file" <?php if($social->Social_Input_Type == 'file') {echo 'selected';} ?>>File</option>
                                                    <option value="email" <?php if($social->Social_Input_Type == 'email') {echo 'selected';} ?>>Email</option>
                                                    <option value="button" <?php if($social->Social_Input_Type == 'button') {echo 'selected';} ?>>Button</option>
                                                    <option value="url" <?php if($social->Social_Input_Type == 'url') {echo 'selected';} ?>>Url</option>
                                                </select>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Hint Type</label>
                                            <div class="col-sm-6">
                                                <select id="hello-single" class="form-control" name="Social_Hint">
                                                    <option value="Please Enter Text">Please Enter Text</option>
                                                    <option value="Please select radio button">Please select radio button</option>
                                                    <option value="Please select checkbox">Please select checkbox</option>
                                                </select>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Order</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="Social_Order" id="name" placeholder="Enter Social Order" value="<?= $social->Social_Order ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social Icon</label>
                                            <div class="col-sm-6">
                                                <input type="file"  name="Social_Icon_URL" id="Social_Icon_URL" >
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <?php 
                                            if($social->Social_Status == 1){
                                                $status = 'on';
                                            }else{
                                                $status = '';
                                            }
                                        ?>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Status </label>
                                            <div class="col-sm-10">
                                                <input type="checkbox" name="Social_Status" class="js-single" <?php echo ($status == 'on') ?  "checked" : "" ;  ?>>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page body end -->
            </div>
        </div>
    </div>
</div>