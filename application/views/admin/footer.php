<!-- Warning Section Ends -->
<!-- Required Jquery -->
<style>
    .selector-toggle {
	display: none;
}
</style>
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/jquery/js/jquery.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/jquery-ui/js/jquery-ui.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/popper.js/js/popper.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"); ?>"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/modernizr/js/modernizr.js"); ?>"></script>
<!-- Chart js -->
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/chart.js/js/Chart.js"); ?>"></script>

<!-- Bootstrap date-time-picker js -->
<script type="text/javascript" src="<?php echo base_url("assets/pages/advance-elements/moment-with-locales.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/pages/advance-elements/bootstrap-datetimepicker.min.js"); ?>"></script>

<!-- data-table js -->
<script src="<?php echo base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") ?>"></script>

<script src="<?php echo base_url("assets/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js") ?>"></script>

<script src="<?php echo base_url("assets/bower_components/datedropper/js/datedropper.min.js") ?>"></script>
<!-- Date-range picker js -->
<script src="<?php echo base_url("assets/bower_components/bootstrap-daterangepicker/js/daterangepicker.js") ?>"></script>
<!-- Date-dropper js -->
<script type="text/javascript" src="<?php echo base_url("assets/bower_components/datedropper/js/datedropper.min.js") ?>"></script>

<script src="<?php echo base_url("assets/pages/data-table/js/jszip.min.js") ?>"></script>
<script src="<?php echo base_url("assets/pages/data-table/js/pdfmake.min.js") ?>"></script>
<script src="<?php echo base_url("assets/pages/data-table/js/vfs_fonts.js") ?>"></script>

<script src="<?php echo base_url("assets/bower_components/datatables.net-buttons/js/buttons.print.min.js") ?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-buttons/js/buttons.html5.min.js") ?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") ?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js") ?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") ?>"></script>

<!-- amchart js -->
<script src="<?php echo base_url("assets/pages/widget/amchart/amcharts.js"); ?>"></script>
<script src="<?php echo base_url("assets/pages/widget/amchart/serial.js"); ?>"></script>
<script src="<?php echo base_url("assets/pages/widget/amchart/light.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/jquery.mCustomScrollbar.concat.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/SmoothScroll.js"); ?>"></script>

<!-- i18next.min.js -->
<script src="<?php echo base_url("assets/pages/user-profile.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/pcoded.min.js"); ?>"></script>

 <!-- ck editor -->
<script src="<?php echo base_url("assets/pages/ckeditor/ckeditor.js"); ?>"></script>

 <!-- echart js -->
<script src="<?php echo base_url("assets/pages/chart/echarts/js/echarts-all.js"); ?>" type="text/javascript"></script>

<script src="<?php echo base_url("assets/bower_components/switchery/js/switchery.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/pages/advance-elements/swithces.js"); ?>"></script>

<!-- custom js -->

<!--<script type="text/javascript" src="<?php echo base_url("assets/js/script.min.js"); ?>"></script>-->
<script src="<?php echo base_url("assets/js/vartical-layout.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/pages/dashboard/custom-dashboard.js"); ?>"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/formvalidate.js") ?>"></script>

<script src="<?php echo base_url("assets/pages/data-table/js/data-table-custom.js") ?>"></script>
<script src="<?php echo base_url("assets/js/sweetalert2.all.min.js") ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/js/script.js") ?>"></script>
<script type="text/javascript">
    $( document ).ready(function() {
      setTimeout(function(){ $('.alert-success').hide(); }, 3000);
    });
</script>
</body>

</html>