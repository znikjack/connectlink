<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><?= $pagetitle ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo base_url('admin/dashboard'); ?>"> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/SocialCategory'); ?>">Social Category</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Inputs Validation start -->
                            <div class="card">
                               <div class="card-block">
                                    <form method="post" action="<?php echo base_url('admin/SocialCategory/form/'.$CategoryData->id) ?>" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social-Category Name </label>
                                            <div class="col-sm-10">
                                                <input required="" type="text" class="form-control" name="Social_Header_Name" id="name" placeholder="Social Header Name" value="<?= $CategoryData->Social_Header_Name ?>">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <?php 
                                            if($CategoryData->Social_Header_Status == 1){
                                                $status = 'on';
                                            }else{
                                                $status = '';
                                            }
                                        ?>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Social-Category Order </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="SocialCategoryOrder" class="form-control" value="<?= $CategoryData->SocialCategoryOrder ?>" required="">
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Status </label>
                                            <div class="col-sm-10">
                                                <input type="checkbox" name="Social_Header_Status" class="js-single" <?php echo ($status == 'on') ?  "checked" : "" ;  ?>>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page body end -->
            </div>
        </div>
    </div>
</div>