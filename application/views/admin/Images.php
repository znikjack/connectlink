<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     */
    function __construct() {
        parent::__construct();
        //$this->load->helper(form);
        $userId = $this->session->userdata('userId');
        if (!isset($userId)) {
            redirect(base_url() . 'Admin');
        }

        $this->load->model('Image_model');
    }

    public function index() {
        $data['imagedata'] = $this->Image_model->get_imageslist();
        $data['page_title'] = "Category Images";
        $this->load->view('header');
        $this->load->view('sidebar', $data);
        $this->load->view('imagelist', $data);
        $this->load->view('footer');
    }

  

    public function add_form() {
        $data['page_title'] = "Add Category Image";
        $data['category'] = $this->Image_model->get_Category();
        $this->load->view('header');
        $this->load->view('sidebar', $data);
        $this->load->view('add_edit_image', $data);
        $this->load->view('footer');
    }
    
    public function edit($id){
        $data['images']     = $this->Image_model->get_images_byID($id);
        $data['page_title'] = "Edit Image";
        $this->load->view('header');
        $this->load->view('sidebar', $data);
        $this->load->view('add_edit_image',$data);
        $this->load->view('footer');
    }

    public function add_edit_process() {
        $image_type = $this->input->post('type');
        $img_id     = $this->input->post('img_id');
        $category_id     = $this->input->post('category_id');
        if (!empty($_FILES['images']['name'])) {
            $filesCount = count($_FILES['images']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['file']['name'] = $_FILES['images']['name'][$i];
                $_FILES['file']['type'] = $_FILES['images']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['images']['error'][$i];
                $_FILES['file']['size'] = $_FILES['images']['size'][$i];

                // File upload configuration
                $uploadPath = 'images/'.$category_id.'/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';

                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                // Upload file to server
                if ($this->upload->do_upload('file')) {
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData = $fileData['file_name'];
                }
                if (!empty($uploadData)) {
                    $save_array['path'] = $uploadData;
                }
                $save_array['id'] = $img_id;
                $save_array['category_id'] = $category_id;
                
                $id = $this->Image_model->insert_image($save_array);
                if (!empty($img_id)) {
                    $this->session->set_flashdata('msg', 'Image has been update successfully.');
                }else{
                    $this->session->set_flashdata('msg', 'Image has been insert successfully.');
                }
            }
        }
        redirect(base_url().'Images');
    }
    public function delete_image($id) {
        $this->Image_model->delete($id);
        $this->session->set_flashdata('msg', 'Image has been deleted successfully.');
        redirect(base_url() . 'Images');
    }

}
