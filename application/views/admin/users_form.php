<style>
    .switchery{margin-left:20px}
    .social_tab .nav-item {width: calc(100% / 5);text-align: center;}
    .social-text{float: right;margin-top: 15px;}
    .social_tab .slide {width: calc(100% / 5);}
</style>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><?php echo $pagetitle; ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo base_url('admin/dashboard') ?>"> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/user') ?>">Users</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">User Profile</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">
                    <!--profile cover start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover-profile">
                                <div class="profile-bg-img">
                                    <?php 
                                        if(!empty($UserData->ProfileURL)){
                                            $UserProfile  = base_url('assets/upload/'.$UserProfile->id.'/UserProfile/'.$UserData->ProfileURL);
                                        }else{
                                            $UserProfile  = base_url('assets/images/user-profile/bg-img1.jpg');
                                        }
                                        if(!empty($UserData->BackgroundURL)){
                                            $Background  = base_url('assets/upload/'.$UserProfile->id.'/UserBackground/'.$UserData->BackgroundURL);
                                        }else{
                                            $Background  = base_url("assets/images/avatar-7.jpg");
                                        }
                                    ?>
                                    <img class="profile-bg-img img-fluid" src="<?= $UserProfile ?>" alt="bg-img">
                                    <div class="card-block user-info">
                                        <div class="col-md-12">
                                            <div class="media-left">
                                                <a href="#" class="profile-image">
                                                    <img class="user-img img-radius" src="<?= $Background ?>" alt="user-img">
                                                </a>
                                            </div>
                                            <div class="media-body row">
                                                <div class="col-lg-12">
                                                    <div class="user-title">
                                                        <h2><?= $UserData->FirstName .' '. $UserData->LastName ?></h2>
                                                        <span class="text-white"><?= $UserData->CompanyName ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--profile cover end-->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- tab header start -->
                            <div class="tab-header card">
                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Social List</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#contacts" role="tab">Interaction</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#review" role="tab">Analysis</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>
                            </div>
                            <!-- tab header end -->
                            <!-- tab content start -->
                            <div class="tab-content">
                                <!-- tab panel personal start -->
                                <div class="tab-pane active" id="personal" role="tabpanel">
                                    <!-- personal card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">About Me</h5>
                                            <button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                <i class="icofont icofont-edit"></i>
                                            </button>
                                        </div>
                                        <div class="card-block">
                                            <div class="view-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="general-info">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <div class="table-responsive">
                                                                        <table class="table m-0">
                                                                            <tbody>
                                                                                <tr>
<!--                                                                                    <input type="checkbox" class="js-primary" checked="">-->
                                                                                    <th scope="row">First Name</th>
                                                                                    <td><?= $UserData->FirstName ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Email Address</th>
                                                                                    <td><?= $UserData->EmailAddress ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Mobile Number</th>
                                                                                    <td><?= $UserData->MobileNumber ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Gender</th>
                                                                                    <td><?= $UserData->gender ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Last Name</th>
                                                                                    <td><?= $UserData->LastName ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Birth Date</th>
                                                                                    <td><?= $UserData->bod ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Location</th>
                                                                                    <td><?= $UserData->address ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">BIO</th>
                                                                                    <td><?= $UserData->Bio ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of general info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of view-info -->
                                            <form action="<?php echo base_url('admin/user/form/'.$UserData->id); ?>" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" id="base_url" value="<?php echo base_url('admin'); ?>" class="base_url">
                                                <div class="edit-info">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                                                            <input type="text" class="form-control" name="FirstName" placeholder="First Name" value="<?= $UserData->FirstName ?>">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-email"></i></span>
                                                                                            <input type="text" class="form-control" name="EmailAddress" placeholder="Email Address" value="<?= $UserData->EmailAddress ?>">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
                                                                                            <input type="text" class="form-control" name="MobileNumber" placeholder="Mobile Number" value="<?= $UserData->MobileNumber ?>">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="form-radio">
                                                                                            <div class="group-add-on">
                                                                                                <div class="radio radiofill radio-inline">
                                                                                                    <label>
                                                                                                        <input type="radio" name="gender" value="Male" <?php echo ($UserData->gender== 'Male') ?  "checked" : "" ;  ?>><i class="helper"></i> Male
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="radio radiofill radio-inline">
                                                                                                    <label>
                                                                                                        <input type="radio" name="gender" value="Female" <?php echo ($UserData->gender== 'Female') ?  "checked" : "" ;  ?>><i class="helper"></i> Female
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon">User Image</span>
                                                                                            <input type="file" class="form-control" name="ProfileURL">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon">Background Image</span>
                                                                                            <input type="file" class="form-control" name="BackgroundURL">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <?php 
                                                                                        if($UserData->PublicMode == 1){
                                                                                            $PublicMode = 'on';
                                                                                        }else{
                                                                                            $PublicMode = '';
                                                                                        }
                                                                                    ?>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon">Public Mode</span>
                                                                                            <input type="checkbox" name="PublicMode" class="js-single" value="<?= $UserData->PublicMode ?>" <?php echo ($PublicMode == 'on') ?  "checked" : "" ;  ?>>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                                                            <input type="text" class="form-control" name="LastName" placeholder="Last Name" value="<?= $UserData->LastName ?>">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
                                                                                            <input type="text" class="form-control" name="address" placeholder="Address" value="<?= $UserData->address ?>">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"><i class="icofont icofont-key"></i></span>
                                                                                            <input type="password" class="form-control" placeholder="Password" name="Password">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="dropper-default" class="form-control" type="text" name="bod" placeholder="Select Your Birth Date" value="<?= $UserData->bod ?>">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="input-group">
                                                                                            <textarea rows="5" cols="5" class="form-control" name="Bio" placeholder="BIO"><?= $UserData->Bio ?></textarea>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                                <div class="text-center">
                                                                    <input type="submit" name="submit" value="Save" class="btn btn-primary waves-effect waves-light m-r-20">
                                                                    <a href="<?php echo base_url('admin/user/add'); ?>" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                                                </div>
                                                            </div>
                                                            <!-- end of edit info -->
                                                        </div>
                                                        <!-- end of col-lg-12 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                            </form>
                                            <!-- end of edit-info -->
                                        </div>
                                        <!-- end of card-block -->
                                    </div>
<!--                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="card-header-text">Description About Me</h5>
                                                    <button id="edit-info-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                        <i class="icofont icofont-edit"></i>
                                                    </button>
                                                </div>
                                                <div class="card-block user-desc">
                                                    <div class="view-desc">
                                                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?" "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able To Do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pain.</p>
                                                    </div>
                                                    <div class="edit-desc">
                                                        <div class="col-md-12">
                                                            <textarea id="description">
                                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?" "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able To Do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pain.</p>
                                                            </textarea>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20 m-t-20">Save</a>
                                                            <a href="#!" id="edit-cancel-btn" class="btn btn-default waves-effect m-t-20">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                    <!-- personal card end-->
                                </div>
                                <!-- tab pane personal end -->
                                <!-- tab pane info start -->
                                <div class="tab-pane" id="binfo" role="tabpanel">
                                    <!-- info card start -->
                                    <div class="card">
                                        <div class="tab-header card">
                                        <ul class="nav nav-tabs md-tabs tab-timeline social_tab" role="tablist" id="mytabview">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#Social" role="tab">Social</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Contact" role="tab">Contact</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Payment" role="tab">Payment</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Music" role="tab">Music</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Other" role="tab">Other</a>
                                                <div class="slide"></div>
                                            </li>
                                        </ul>
                                    </div>
                                        <div class="tab-pane" id="header1" role="tabpanel">
                                            <div class="card-header">
                                                <h5 class="card-header-text">User Services</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="card b-l-success business-info services m-b-20">
                                                            <div class="card-header">
                                                                <div class="service-header">
                                                                    <a href="#">
                                                                        <h3 class="card-header-text">Facebook</h3>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <img src="<?php echo base_url('upload/social_icon/facebook.png'); ?>" style="width: 10%;">
                                                                        <input type="text" class="form-control social-text col-md-10" value="https://www.facebook.com/campaign/landing.php?campaign_id=1653993517">
                                                                    </div>
                                                                    <!-- end of col-sm-8 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>
                                                            <!-- end of card-block -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="card b-l-danger business-info services">
                                                            <div class="card-header">
                                                                <div class="service-header">
                                                                    <a href="#">
                                                                        <h3 class="card-header-text">Twitter</h3>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <img src="<?php echo base_url('upload/social_icon/twitter.jpeg'); ?>" style="width: 10%;">
                                                                         <input type="text" class="form-control social-text col-md-10" value="https://twitter.com/?lang=en">
                                                                    </div>
                                                                    <!-- end of col-sm-8 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>
                                                            <!-- end of card-block -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     
                                    <!-- info card end -->
                                </div>
                               
                                <!-- tab pane info end -->
                                <!-- tab pane contact start -->
                                <div class="tab-pane" id="contacts" role="tabpanel">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4 class="card-title">Contacts<span class="f-15"> (100)</span></h4>
                                                </div>
                                                <div class="card-block">
                                                    <div class="connection-list">
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-1.jpg'); ?>" alt="f-1" data-toggle="tooltip" data-placement="top" data-original-title="Airi Satou">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-2.jpg'); ?>" alt="f-2" data-toggle="tooltip" data-placement="top" data-original-title="Angelica Ramos">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-3.jpg'); ?>" alt="f-3" data-toggle="tooltip" data-placement="top" data-original-title="Ashton Cox">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-4.jpg'); ?>" alt="f-4" data-toggle="tooltip" data-placement="top" data-original-title="Cara Stevens">
                                                        </a>
                                                        <a href="#"><img class="img-fluid img-radius" src="<?php echo base_url('assets\images\user-profile\follower\f-5.jpg'); ?>" alt="f-5" data-toggle="tooltip" data-placement="top" data-original-title="Garrett Winters">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- user contact card left side end -->
                                        </div>
                                    </div>
                                </div>
                                <!-- tab pane contact end -->
                                <div class="tab-pane" id="review" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Review</h5>
                                        </div>
                                        <div class="card-block">
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left">
                                                        <a href="#">
                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-1.jpg'); ?>" alt="Generic placeholder image">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="media-heading">Sortino media<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                        <div class="stars-example-css review-star">
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                            <i class="icofont icofont-star"></i>
                                                        </div>
                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                        <div class="m-b-25">
                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                        </div>
                                                        <hr>
                                                        <!-- Nested media object -->
                                                        <div class="media mt-2">
                                                            <a class="media-left" href="#">
                                                                <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-2.jpg'); ?>" alt="Generic placeholder image">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                                <!-- Nested media object -->
                                                                <div class="media mt-2">
                                                                    <div class="media-left">
                                                                        <a href="#">
                                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-3.jpg'); ?>" alt="Generic placeholder image">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                        <div class="stars-example-css review-star">
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                        </div>
                                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                        <div class="m-b-25">
                                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Nested media object -->
                                                        <div class="media mt-2">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-1.jpg'); ?>" alt="Generic placeholder image">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Cedric Kelly<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="media mt-2">
                                                            <a class="media-left" href="#">
                                                                <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-4.jpg'); ?>" alt="Generic placeholder image">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                                <!-- Nested media object -->
                                                                <div class="media mt-2">
                                                                    <div class="media-left">
                                                                        <a href="#">
                                                                            <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-3.jpg'); ?>" alt="Generic placeholder image">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                        <div class="stars-example-css review-star">
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                            <i class="icofont icofont-star"></i>
                                                                        </div>
                                                                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                        <div class="m-b-25">
                                                                            <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="media mt-2">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object img-radius comment-img" src="<?php echo base_url('assets\images\avatar-2.jpg'); ?>" alt="Generic placeholder image">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="media-heading">Mark Doe<span class="f-12 text-muted m-l-5">Just now</span></h6>
                                                                <div class="stars-example-css review-star">
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                    <i class="icofont icofont-star"></i>
                                                                </div>
                                                                <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                <div class="m-b-25">
                                                                    <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Right addon">
                                                <span class="input-group-addon"><i class="icofont icofont-send-mail"></i></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- tab content end -->
                        </div>
                    </div>
                </div>
                <!-- Page-body end -->
            </div>
        </div>
    </div>
</div>