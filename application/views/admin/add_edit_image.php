<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $page_title ?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'Dashboard' ?>"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active"> <?= $page_title ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $mode ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'Images/add_edit_process' ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $images->id; ?>" name="img_id">
                        <div class="box-body">
                             <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="category">Category Name</label>
                                    <select name="category_id" class="form-control">
                                        <?php foreach ($category as $row) { ?>
                                        <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputFile"></label>
                                    <input type="file" name="images[]" id="exampleInputFile" multiple required="">
                                    <?php if (!empty($images->path)) { ?>
                                        <img src="<?php echo base_url($images->path); ?>" width="80" height="80" />
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>  
    </section>
</div>
     