<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><?php echo $pagetitle; ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <a href="<?php echo base_url('admin/user/add'); ?>" class="btn btn-primary" id="primary-popover-content">Add User</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-header top-main-userlist">
                    <?php
                    $message = $this->session->flashdata('message');
                    if (isset($message) AND $message != '') {
                        ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled"></i>
                            </button>
                            <?= $message ?>
                        </div>
                    <?php } ?>  
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body usersinfo">
                    <!-- DOM/Jquery table start -->
                    <div class="card">
                        <div class="card-block">
                            <div class="table-responsive dt-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>App-Version</th>
                                            <th>Phone-Verify</th>
                                            <th>Email-Verify</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($users)): ?>
                                            <?php foreach ($users as $row): ?>
                                                <tr class="remove_<?= $row->id ?>">
                                                    <td><?php echo $row->id; ?></td>
                                                    <td><?php echo $row->FirstName .' '. $row->LastName; ?></td>
                                                    <td><?php echo $row->EmailAddress; ?></td>
                                                    <td><?php echo $row->MobileNumber; ?></td>
                                                    <td><?php echo $row->AppVersion; ?></td>
                                                    <td><?php echo $row->IsEmailVerify; ?></td>
                                                    <td><?php echo $row->IsMobileVerify; ?></td>
                                                    <td><?php echo $row->Status; ?></td>
                                                    <td>
                                                        <a class="btn btn-success btn-icon" href="<?php echo base_url('admin/user/form/' . $row->id); ?>"><i class="feather icon-edit"></i></a>
                                                        <a class="btn btn-danger btn-icon" href="#" onclick="return areyousure(<?= $row->id ?>);"><i class="feather icon-trash"></i></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row Created Callback table end -->
        </div>
        <!-- Page-body start -->
    </div>
</div>
<!-- Main-body end -->

<div id="styleSelector">

</div>
<script type="text/javascript">
    function areyousure(userId)
    {
       swal.fire({
        title: 'Are yor sure want to delete?',
        text: "This User!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/User/delete'); ?>",
                data: {id:userId} ,
                dataType: "json",
                success: function (response) {
                    if(response.status == 'true'){
                        Swal.fire(
                        'Deleted!',
                         response.message,
                        'success'
                      )
                      $('.remove_'+userId).remove();
                    }
                }
            });
        }
        });
    }
</script>