<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->helper(form);
        $this->load->model('User_model');
    }

    public function index() {
        $data['pagetitle'] = "Users Listing";
        $data['users'] = $this->User_model->GetSelectData('users','id','desc');
        $this->load->view('admin/header');
        $this->load->view('admin/users',$data);
        $this->load->view('admin/footer');
    }
    public function add() {
        $data['pagetitle'] = "Users Form";
        $this->load->view('admin/header');
        $this->load->view('admin/users_form_add',$data);
        $this->load->view('admin/footer');
    }
    public function form($id = false) {
        $data['pagetitle'] = "Users Form";
        if ($id) {
           $data['UserData']	= $this->User_model->GetUserInfo('users','id',$id);
        }
        $Postdata = $this->input->post();
        if (array_key_exists("PublicMode",$Postdata)){
            $PublicMode = 1;
        }else{
            $PublicMode = 0;
        }
       
        if ($Postdata) {
            $save['id']                 = $id;
            $save['FirstName']          = $Postdata['FirstName'];
            $save['LastName']           = $Postdata['LastName'];
            $save['MobileNumber']       = $Postdata['MobileNumber'];
            $save['EmailAddress']       = $Postdata['EmailAddress'];
            $save['gender']             = $Postdata['gender'];
            $save['address']            = $Postdata['address'];
            $save['bod']                = $Postdata['bod'];
            $save['Bio']                = $Postdata['Bio'];
            $save['PublicMode']         = $PublicMode;
           
            if ($this->input->post('Password') != '' || !$id) {
                $save['Password'] = sha1($Postdata['Password']);
            }
            $user_id = $this->User_model->save('users', $save);
            if ($user_id) {
                $path = "./upload/$user_id";
                $UserProfilePath = "./upload/$user_id/UserProfile";
                $UserResumePath = "./upload/$user_id/Resume";
                $UserVideoPath = "./upload/$user_id/Video";
                $UserBackgroundPath = "./upload/$user_id/UserBackground";

                if (!is_dir($path)) {
                    mkdir($path, 0777);
                }
                if (!is_dir($UserProfilePath)) {
                    mkdir($UserProfilePath, 0777, true);
                }
                if (!is_dir($UserResumePath)) {
                    mkdir($UserResumePath, 0777, true);
                }
                if (!is_dir($UserVideoPath)) {
                    mkdir($UserVideoPath, 0777, true);
                }
                if (!is_dir($UserBackgroundPath)) {
                    mkdir($UserBackgroundPath, 0777, true);
                }
                
                
                if (isset($_FILES['ProfileURL']['name']) AND $_FILES['ProfileURL']['name'] != '') {
                    $uploads['upload_path'] = "./upload/$user_id/UserProfile";
                    $uploads['allowed_types'] = 'jpg|png|gif|jpeg';
                    $uploads['overwrite'] = FALSE;
                    $uploads['file_name'] = $_FILES['ProfileURL']['name'];
                    $this->load->library('upload', $uploads);
                    $this->upload->initialize($uploads);
                    if (!$this->upload->do_upload('ProfileURL')) {
                        echo $this->upload->display_errors();
                    } else {
                        $img_array['file'] = $this->upload->data();
                        $save_update['ProfileURL'] = $img_array['file']['file_name'];
                    }
                }
                if (isset($_FILES['BackgroundURL']['name']) AND $_FILES['BackgroundURL']['name'] != '') {
                    $uploads['upload_path'] = "./upload/$user_id/UserBackground";
                    $uploads['allowed_types'] = 'jpg|png|gif|jpeg';
                    $uploads['overwrite'] = FALSE;
                    $uploads['file_name'] = $_FILES['BackgroundURL']['name'];
                    $this->load->library('upload', $uploads);
                    $this->upload->initialize($uploads);
                    if (!$this->upload->do_upload('BackgroundURL')) {
                        echo $this->upload->display_errors();
                    } else {
                        $img_array['file'] = $this->upload->data();
                        $save_update['user_image'] = $img_array['file']['file_name'];
                    }
                }
                $save_update['id'] = $user_id;
                $this->User_model->save('users', $save);
            }

            $this->session->set_flashdata('message', 'User Register Successfully Submitted...!');
            redirect('admin/user');
        }
        $this->load->view('admin/header');
        $this->load->view('admin/users_form',$data);
        $this->load->view('admin/footer');
    }
    function checkEmail(){
        $email = $this->input->post('EmailAddress');
        $this->User_model->check_emailID($email);
    }
    public function profile($id = '') {
        $data['pagetitle'] = "User Profile";
        $UserInfo = $this->User_model->GetUserInfo('users','id',$id);
        $this->load->view('admin/header');
        $this->load->view('admin/users_profile',$data);
        $this->load->view('admin/footer');
    }
    public function delete() {
        $id = $this->input->post('id');
        if ($id) {
            $delete = $this->User_model->delete('users','id',$id);
            $this->session->set_flashdata('message', 'User has been deleted successfully.');
            echo json_encode(array('status' => 'true', 'message' => 'User has been deleted successfully.'));
        }else{
            echo json_encode(array('status' => 'true', 'message' => 'Please try again.'));
        }
    }

}
