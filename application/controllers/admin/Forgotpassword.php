<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Sys_model');
    }

    public function index() {
        $PostArr = $this->input->post();
        if (!empty($PostArr)) {
            $this->load->helper('string');
            $email = $this->input->post('email');
            $reset = $this->Sys_model->reset_admin_password($email);
            if ($reset) {
                $this->session->set_flashdata('message', 'A new password has been generated and sent to your email.');
                redirect('admin/login');
            } else {
                $this->session->set_flashdata('error', 'There is no record of your account.');
            }
        }
        $this->load->view('admin/forgotpassword');
    }

}
