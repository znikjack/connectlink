<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->helper(form);
        $this->load->model('SocialHeader_model');
    }

    public function index() {
        $data['pagetitle'] = "Social Listing";
        $data['social_category'] = $this->SocialHeader_model->GetAllSocial('social_list','id','desc');
        $this->load->view('admin/header');
        $this->load->view('admin/social_listing',$data);
        $this->load->view('admin/footer');
    }
    public function form($id = false) {
        $data['pagetitle'] = "Social Form";
        $data['social_category'] = $this->SocialHeader_model->GetAllCategorys('social_header','id','desc');
        if ($id) {
            $data['social'] = $this->SocialHeader_model->GetCategoryById('social_list', 'id', $id);
        }
        $Postdata = $this->input->post();
        if (array_key_exists("Social_Status",$Postdata)){
            $status = 1;
        }else{
            $status = 0;
        }
        if ($Postdata) {
            $save['id'] = $id;
            $save['Social_Header_ID']   = $Postdata['Social_Header_ID'];
            $save['Social_Title']       = $Postdata['Social_Title'];
            $save['Social_Name']        = $Postdata['Social_Name'];
            $save['Social_Class'] = $Postdata['Social_Class'];
            $save['Social_PlaceHolder'] = $Postdata['Social_PlaceHolder'];
            $save['Social_Hint']        = $Postdata['Social_Hint'];
            $save['Social_Input_Type']  = $Postdata['Social_Input_Type'];
            $save['Social_Type']        = $Postdata['Social_Type'];
            $save['Social_Order']        = $Postdata['Social_Order'];
            $save['Social_Status']      = $status;
            
            if(isset($_FILES['Social_Icon_URL']['name']) AND $_FILES['Social_Icon_URL']['name'] != ''){
                $uploads['upload_path'] = 'upload/category_icon/';
                $uploads['allowed_types'] = 'jpg|png|gif|zip|pdf';
                $uploads['overwrite'] = FALSE;
                $uploads['file_name'] = $_FILES['Social_Icon_URL']['name'];
                $this->load->library('upload',$uploads);
                if(!$this->upload->do_upload('Social_Icon_URL'))
                {
                    echo $this->upload->display_errors();
                }
                else
                {
                    $img_array['file']= $this->upload->data();
                    $save['Social_Icon_URL']=$img_array['file']['file_name'];
                }	
            }	
            
            $this->SocialHeader_model->save('social_list', $save);
            $this->session->set_flashdata('message', 'Successfully');
            redirect('admin/Social');
        }
        $this->load->view('admin/header');
        $this->load->view('admin/social_form', $data);
        $this->load->view('admin/footer');
    }

    public function delete() {
        $id = $this->input->post('id');
        if ($id) {
            $delete = $this->SocialHeader_model->delete('social_header','id',$id);
            $this->session->set_flashdata('message', 'Category has been deleted successfully.');
            echo json_encode(array('status' => 'true', 'message' => 'Category has been deleted successfully.'));
        }
    }

}
