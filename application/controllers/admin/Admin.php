<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        //$this->load->helper(form);
        $this->load->model('Sys_model');
//        $userId = $this->session->userdata('userId');
//        if (!isset($userId)) {
//            redirect(base_url() . 'Admin');
//        }
    }

    public function index() {

        $this->load->view('admin/login');
    }

    public function login() {
        $name = $this->input->post('name');
        $password = sha1($this->input->post('password'));

        $users = $this->Sys_model->check_user('admin', 'name', $name, 'password', $password);
        if (!empty($users)) {
            $this->session->set_userdata('userId', $users->id);
            $this->session->set_userdata('userName', $users->name);
            redirect(base_url() . 'admin/dashboard');
        } else {

            $data['validation_message'] = "Invalid Username And Password";
            $this->load->view('admin/login', $data);
        }
    }

    public function logout() {

        $this->session->unset_userdata('userId');
        $this->session->unset_userdata('userName');
        $this->load->view('admin/login');
    }

}
