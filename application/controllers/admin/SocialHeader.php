<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SocialHeader extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->helper(form);
        $this->load->model('SocialHeader_model');
    }

    public function index() {
        $data['pagetitle'] = "Social Category Listing";
        $data['social_category'] = $this->SocialHeader_model->GetAllCategorys('Social_Header','id','desc');
        $this->load->view('admin/header');
        $this->load->view('admin/social_categorys_listing',$data);
        $this->load->view('admin/footer');
    }
    public function form($id = false) {
        $data['pagetitle'] = "Social Category Form";
        if ($id) {
           $data['CategoryData']	= $this->SocialHeader_model->GetCategoryById('Social_Header','id',$id);
        }
        $Postdata = $this->input->post();
        if ($Postdata) {
            $save['id']                       = $id;
            $save['Social_Header_Name']       = $Postdata['Social_Header_Name'];
            $save['Social_Header_Status']     = 1;
            $this->SocialHeader_model->save('Social_Header',$save);
            $this->session->set_flashdata('message', 'Successfully');
            redirect('admin/SocialHeader');
        }
        $this->load->view('admin/header');
        $this->load->view('admin/social_category_form',$data);
        $this->load->view('admin/footer');
    }
    public function delete() {
        $id = $this->input->post('id');
        if ($id) {
            $delete = $this->SocialHeader_model->delete('Social_Header','id',$id);
            $this->session->set_flashdata('message', 'Category has been deleted successfully.');
            echo json_encode(array('status' => 'true', 'message' => 'Category has been deleted successfully.'));
        }
    }

}
