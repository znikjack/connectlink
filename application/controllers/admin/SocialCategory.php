<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SocialCategory extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->helper(form);
        $this->load->model('SocialHeader_model');
    }

    public function index() {
        $data['pagetitle'] = "Social Category Listing";
        $data['social_category'] = $this->SocialHeader_model->GetAllCategorys('social_header','id','desc');
        $this->load->view('admin/header');
        $this->load->view('admin/social_categorys_listing',$data);
        $this->load->view('admin/footer');
    }
    public function form($id = false) {
        $data['pagetitle'] = "Social Category Form";
        if ($id) {
           $data['CategoryData']	= $this->SocialHeader_model->GetCategoryById('social_header','id',$id);
        }
        $Postdata = $this->input->post();
        if (array_key_exists("Social_Header_Status",$Postdata)){
            $status = 1;
        }else{
            $status = 0;
        }
        if ($Postdata) {
            $save['id']                       = $id;
            $save['Social_Header_Name']       = $Postdata['Social_Header_Name'];
            $save['SocialCategoryOrder']       = $Postdata['SocialCategoryOrder'];
            $save['Social_Header_Status']     = $status;
            $this->SocialHeader_model->save('social_header',$save);
            $this->session->set_flashdata('message', 'Category Insert Successfully...!');
            redirect('admin/SocialHeader');
        }
        $this->load->view('admin/header');
        $this->load->view('admin/social_category_form',$data);
        $this->load->view('admin/footer');
    }
    public function delete() {
        $id = $this->input->post('id');
        if ($id) {
            $delete = $this->SocialHeader_model->delete('social_header','id',$id);
            $this->session->set_flashdata('message', 'Category has been deleted successfully.');
            echo json_encode(array('status' => 'true', 'message' => 'Category has been deleted successfully.'));
        }
    }

}
