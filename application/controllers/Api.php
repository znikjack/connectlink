<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Apiservice_model');
    }

    public function index() {
        $status = false;
        $message = 'ConnectLink!';
        $user_detials = null;
        echo json_encode(array('message' => $message, 'status' => $status, 'detail' => $detials));
    }

    /* Login API */

    public function login() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $message = 'Please insert parameter...!';
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata['EmailAddress'] && $Postdata['Password']) {
            $UserInfo = $this->Apiservice_model->get_check_login('users', 'EmailAddress', $Postdata['EmailAddress'], 'Password', $Postdata['Password']);
            if (!empty($UserInfo)) {
                $status = true;
                $message = 'Your login is successfully...!';
                $detail = $UserInfo;
            } else {
                $status = false;
                $message = 'Login failed please try again...!';
                $detail = null;
            }
        }
        echo json_encode(array('message' => $message, 'status' => $status, 'detail' => $detail));
    }

    public function registration() {
        error_reporting(1);
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $message = 'Failed!';
        $detail = null;
        $Postdata = file_get_contents("php://input");
        $Postdata = json_decode($Postdata, true);
        if ($Postdata) {
            $otp = rand(1111, 9999);
            $check_user = $this->Apiservice_model->AlreadyUserExitsCheck('users', 'EmailAddress', $Postdata['EmailAddress']);
            if (!empty($check_user)) {
                $status = false;
                $message = 'Already registered email id so use for another...!';
                $detail = null;
            } else {

                $save['FirstName'] = $Postdata['FirstName'];
                $save['LastName'] = $Postdata['LastName'];
                $save['UserName'] = $Postdata['UserName'];
                $save['MobileNumber'] = $Postdata['MobileNumber'];
                $save['EmailAddress'] = $Postdata['EmailAddress'];
                $save['Password'] = sha1($Postdata['Password']);
                $save['FCMToken'] = $Postdata['FCMToken'];
                $save['DeviceType'] = $Postdata['DeviceType'];
                $save['UserType'] = $Postdata['UserType'];
                $save['AppVersion'] = $Postdata['AppVersion'];
                $user_id = $this->Apiservice_model->InsertData('users', $save);
                $save['user_id'] = $user_id;

                $path = "./upload/$user_id";
                $UserProfilePath = "./upload/$user_id/UserProfile";
                $UserResumePath = "./upload/$user_id/Resume";
                $UserVideoPath = "./upload/$user_id/Video";

                if (!is_dir($path)) {
                    mkdir($path, 0777);
                }
                if (!is_dir($UserProfilePath)) {
                    mkdir($UserProfilePath, 0777, true);
                }
                if (!is_dir($UserResumePath)) {
                    mkdir($UserResumePath, 0777, true);
                }
                if (!is_dir($UserVideoPath)) {
                    mkdir($UserVideoPath, 0777, true);
                }

                $status = true;
                $message = 'Your registeration successfully...!';
                $detail = $save;
            }
        } else {
            $status = false;
            $message = 'Please insert register fileds...!';
            $detail = null;
        }
        echo json_encode(array('message' => $message, 'status' => $status, 'detail' => $detail));
    }

    public function check_otp() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $message = 'Failed!';
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata) {
            $userInfo = $this->Apiservice_model->check_otp_pass('users', 'otp', $Postdata['IsMobileVerify'], 'id', $Postdata['user_id']);
            if ($userInfo) {
                $status = true;
                $message = 'Successfully otp match...!';
                $detail = $userInfo;
            } else {
                $status = false;
                $message = 'Your otp is not match...!';
                $detail = null;
            }
        } else {
            $status = false;
            $message = 'Please insert data...!';
            $detail = null;
        }
        echo json_encode(array('message' => $message, 'status' => $status, 'detail' => $detail));
    }

    public function GetUserProfile() {
        $status = false;
        $message = "Please insert user id...!";
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata) {
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('users', 'id', $Postdata['UserID']);
            if (!empty($Userdata)) {
                $status = true;
                $message = 'success';
                $detail = $Userdata;
            } else {
                $status = false;
                $message = 'This user data is not found...!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }

    public function ProfileUpload() {
        $status = false;
        $message = "Please insert user id...!";
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata) {
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('users', 'id', $Postdata['UserID']);
            if (isset($_FILES['Image']['name']) AND $_FILES['Image']['name'] != '') {
                $userId = $Postdata['UserID'];
                $uploads['upload_path'] = "upload/$userId/UserProfile";
                $uploads['allowed_types'] = 'jpg|png|gif|zip|pdf';
                $uploads['overwrite'] = FALSE;
                $uploads['file_name'] = $_FILES['Image']['name'];
                $this->load->library('upload', $uploads);
                if (!$this->upload->do_upload('Image')) {
                    echo $this->upload->display_errors();
                } else {
                    $img_array['file'] = $this->upload->data();
                    $updatedata['UserImage'] = $img_array['file']['file_name'];
                    $this->Apiservice_model->UpdateUserProfileDataByID('users', $updatedata, 'id', $Postdata['UserID']);
                }
            }
            $status = true;
            $message = 'Your image successfully uploaded...!';
            $detail = $Userdata;
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }

    public function EditProfile() {
        $status = false;
        $message = 'Please insert data...!';
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata) {
            $save['Flag'] = $Postdata['Flag'];
            $save['Username'] = $Postdata['Username'];
            $save['FirstName'] = $Postdata['FirstName'];
            $save['LastName'] = $Postdata['LastName'];
            $save['EmailAddress'] = $Postdata['EmailAddress'];
            $save['MobileNumber'] = $Postdata['MobileNumber'];
            $save['Bio'] = $Postdata['Bio'];
            $save['CompanyName'] = $Postdata['CompanyName'];
            $save['Title'] = $Postdata['Title'];
            $save['Genders'] = $Postdata['Genders'];
            $save['Dob'] = $Postdata['Dob'];
            $save['Address'] = $Postdata['Address'];
            $save['Lat'] = $Postdata['Lat'];
            $save['Long'] = $Postdata['Long'];
            $result = $this->Apiservice_model->UpdateUserProfileDataByID('users', $save, 'id', $Postdata['UserID']);
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('users', 'id', $Postdata['UserID']);
            if ($result) {
                $status = true;
                $message = 'Your profile is update successfully....!';
                $detail = $Userdata;
            } else {
                $status = false;
                $message = 'Your profile is not update....!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }

    public function ChangePassword() {
        $status = false;
        $message = 'Please insert data...!';
        $detail = null;
        $Postdata = file_get_contents("php://input");
        $Postdata = json_decode($Postdata, true);
        if ($Postdata) {
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('users', 'id', $Postdata['UserID']);
            if (sha1($Postdata['OldPassword']) == $Userdata->Password) {
                $save['Password'] = sha1($Postdata['NewPassword']);
                $this->Apiservice_model->UpdateUserProfileDataByID('users', $save, 'id', $Postdata['UserID']);
                $status = false;
                $message = 'Your password is change successfully...!';
                $detail = $Userdata;
            } else {
                $status = false;
                $message = 'Your old password is not match ....!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }
    
    public function Forgotpassword() {
        $status = false;
        $message = 'Please insert email...!';
        $detail = null;
        $Postdata = file_get_contents("php://input");
        $Postdata = json_decode($Postdata, true);
        if ($Postdata) {
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('users', 'EmailAddress', $Postdata['email']);
            if(!empty($Userdata)){
                $this->load->helper('string');
                $new_password       = random_string('alnum', 8);
                $data['password']   = sha1($new_password);
                $this->Apiservice_model->reset_password($Postdata['email'],$data);
                
                $status = true;
                $message = 'Password has been sent to your email';
                $detail = $Userdata;
            }else{
                $status = false;
                $message = 'This email is not exits...!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }

    public function getProduct() {
        $status = false;
        $message = 'Please insert data...!';
        $detail = null;
        $total_image = 0;
        $limit = 0;
        if ($this->input->post()) {
            $cat_ID = $this->input->post('category_id');
            $postdata = $this->input->post();
            $limit = ($postdata['limit']) ? $postdata['limit'] : '20';
            $ofs = ($postdata['offset']) ? $postdata['offset'] : '0';
            $offset = ($ofs != 0) ? ($limit * $ofs) : 0;

            $data = $this->Image_model->get_imageslist_by_ID($cat_ID, $limit, $offset);
            $total_image = count($this->Image_model->get_imageslist_by_ID($cat_ID, '', '', 'onlycount'));
            foreach ($data as $key => $row) {
                $data[$key]->image_url = base_url('images/' . $row->category_id . '/' . $row->path);
            }
            if ($data != false && count($data) > 0) {
                $status = true;
                $message = 'success';
                $detail = $data;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail, 'total_count' => $total_image, 'limit' => $limit));
    }

}
