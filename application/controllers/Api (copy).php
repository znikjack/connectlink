<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Apiservice_model');
    }

    public function index() {
        $status = false;
        $msg = 'ConnectLink!';
        $user_detials = null;
        echo json_encode(array('message' => $msg, 'status' => $status, 'detail' => $detials));
    }

    /* Login API */

    public function login() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $msg = 'Please insert parameter...!';
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata['EmailAddress'] && $Postdata['Password']) {
            $UserInfo = $this->Apiservice_model->get_check_login('Users', 'EmailAddress', $Postdata['EmailAddress'], 'Password', $Postdata['Password']);
            if (!empty($UserInfo)) {
                $status = true;
                $msg = 'Your login is successfully...!';
                $detail = $UserInfo;
            } else {
                $status = false;
                $msg = 'Login failed please try again...!';
                $detail = null;
            }
        }
        echo json_encode(array('message' => $msg, 'status' => $status, 'detail' => $detail));
    }
    
    public function registration() {
        error_reporting(1);
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $msg    = 'Failed!';
        $detail = null;
        $Postdata   = file_get_contents("php://input");
        $Postdata   = json_decode($Postdata,true);
        if ($Postdata) {
            $otp = rand(1111, 9999);
            $check_user = $this->Apiservice_model->AlreadyUserExitsCheck('Users', 'EmailAddress', $Postdata['EmailAddress']);
            if (!empty($check_user)) {
                $status = false;
                $msg = 'Already registered email id so use for another...!';
                $detail = null;
            }else{
                $save['FirstName']          = $Postdata['FirstName'];
                $save['LastName']           = $Postdata['LastName'];
                $save['UserName']           = $Postdata['UserName'];
                $save['MobileNumber']       = $Postdata['MobileNumber'];
                $save['EmailAddress']       = $Postdata['EmailAddress'];
                $save['Password']           = sha1($Postdata['Password']);
                $save['FCMToken']           = $Postdata['FCMToken'];
                $save['DeviceType']         = $Postdata['DeviceType'];
                $save['UserType']           = $Postdata['UserType'];
                $save['IsMobileVerify']     = $otp;
                $user_id = $this->Apiservice_model->InsertData('Users', $save);
                $save['user_id'] = $user_id;
                $status = true;
                $msg    = 'Your registeration successfully...!';
                $detail = $save;
            }
        } else {
            $status = false;
            $msg = 'Please insert register fileds...!';
            $detail = null;
        }
        echo json_encode(array('message' => $msg, 'status' => $status, 'detail' => $detail));
    }
    
    public function check_otp(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
        $status = false;
        $msg = 'Failed!';
        $detail = null;
        $Postdata = $this->input->post();
        if ($Postdata) {
            $userInfo = $this->Apiservice_model->check_otp_pass('Users', 'otp', $Postdata['IsMobileVerify'], 'id', $Postdata['user_id']);
            if($userInfo){
                $status = true;
                $msg    = 'Successfully otp match...!';
                $detail = $userInfo;
            }else{
                $status = false;
                $msg = 'Your otp is not match...!';
                $detail = null;
            }
        }else {
            $status = false;
            $msg = 'Please insert data...!';
            $detail = null;
        }
        echo json_encode(array('message' => $msg, 'status' => $status, 'detail' => $detail));
    }

    public function GetUserProfile() {
        $status = false;
        $message = "Please insert user id...!";
        $detail = null;
        $Postdata = $this->input->post();
        if($Postdata){
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('Users','id',$Postdata['UserID']);
            if (!empty($Userdata)) {
                $status = true;
                $message = 'success';
                $detail = $Userdata;
            }else{
                $status = false;
                $message = 'This user data is not found...!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }
    
    public function ProfileUpload() {
        $status = false;
        $message = "Please insert user id...!";
        $detail = null;
        $Postdata = $this->input->post();
        if($Postdata){
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('Users','id',$Postdata['UserID']);
            if (!empty($this->input->post('UserImage'))) {
                $base64_string = $this->input->post('UserImage');
                $data = explode(',', $base64_string);
                $pos  = strpos($base64_string, ';');
                $type = explode(':', substr($base64_string, 0, $pos))[1];
                $image_ext = explode('/',$type); 
                $file_name = 'user'.date('mis').'.'.$image_ext[1];
                $ifp = file_put_contents(FCPATH.'upload/user_images/'.$file_name, base64_decode($data[1])); 
                $updatedata['UserImage'] = isset($file_name) ? $file_name : '';
                $this->Apiservice_model->UpdateUserProfileDataByID('Users',$updatedata,'id',$Postdata['UserID']);
                
                $status = true;
                $message = 'Your image successfully uploaded...!';
                $detail = $Userdata;
            }else{
                $status = false;
                $message = 'Your image is not uploaded...!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }
    
    
    
    public function EditProfile() {
        $status = false;
        $message = 'Please insert data...!';
        $detail = null;
        $Postdata = $this->input->post();
        if($Postdata){
            $save['Flag'] = $Postdata['Flag'];
            $save['Username'] = $Postdata['Username'];
            $save['FirstName'] = $Postdata['FirstName'];
            $save['LastName'] = $Postdata['LastName'];
            $save['EmailAddress'] = $Postdata['EmailAddress'];
            $save['MobileNumber'] = $Postdata['MobileNumber'];
            $save['Bio'] = $Postdata['Bio'];
            $save['CompanyName'] = $Postdata['CompanyName'];
            $save['Title'] = $Postdata['Title'];
            $save['Genders'] = $Postdata['Genders'];
            $save['Dob'] = $Postdata['Dob'];
            $save['Address'] = $Postdata['Address'];
            $save['Lat'] = $Postdata['Lat'];
            $save['Long'] = $Postdata['Long'];
            $result = $this->Apiservice_model->UpdateUserProfileDataByID('Users',$save,'id',$Postdata['UserID']);
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('Users','id',$Postdata['UserID']);
            if($result){
                $status = true;
                $message = 'Your profile is update successfully....!';
                $detail = $Userdata;
            }else{
                $status = false;
                $message = 'Your profile is not update....!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }
    public function ChangePassword() {
        $status = false;
        $msg = 'Please insert data...!';
        $detail = null;
        $Postdata   = file_get_contents("php://input");
        $Postdata   = json_decode($Postdata,true);
        if($Postdata){
            $Userdata = $this->Apiservice_model->GetUserProfileDataByID('Users','id',$Postdata['UserID']);
            if(sha1($Postdata['OldPassword']) == $Userdata->Password){
                $save['Password']           = sha1($Postdata['NewPassword']);
                $this->Apiservice_model->UpdateUserProfileDataByID('Users',$save,'id',$Postdata['UserID']);
                $status = false;
                $message = 'Your password is change successfully...!';
                $detail = $Userdata;
            }else{
                $status = false;
                $message = 'Your old password is not match ....!';
                $detail = null;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail));
    }

    public function getProduct() {
        $status = false;
        $msg = 'Please insert data...!';
        $detail = null;
        $total_image = 0;
        $limit = 0;
        if ($this->input->post()) {
            $cat_ID = $this->input->post('category_id');
            $postdata = $this->input->post();
            $limit = ($postdata['limit']) ? $postdata['limit'] : '20';
            $ofs = ($postdata['offset']) ? $postdata['offset'] : '0';
            $offset = ($ofs != 0) ? ($limit * $ofs) : 0;

            $data = $this->Image_model->get_imageslist_by_ID($cat_ID, $limit, $offset);
            $total_image = count($this->Image_model->get_imageslist_by_ID($cat_ID, '', '', 'onlycount'));
            foreach ($data as $key => $row) {
                $data[$key]->image_url = base_url('images/' . $row->category_id . '/' . $row->path);
            }
            if ($data != false && count($data) > 0) {
                $status = true;
                $message = 'success';
                $detail = $data;
            }
        }
        echo json_encode(array('status' => $status, 'message' => $message, 'detail' => $detail, 'total_count' => $total_image, 'limit' => $limit));
    }
    
    function testfolder(){
        $path   = './upload/2';
        $UserProfilePath   = './upload/2/UserProfile';
        $UserResumePath   = './upload/2/Resume';
        $UserVideoPath   = './upload/2/Video';

        if (!is_dir($path)) { 
            mkdir($path,0777,true);
        }
        if (!is_dir($UserProfilePath)) { 
            mkdir($UserProfilePath,0777,true);
        }
        if (!is_dir($UserResumePath)) { 
            mkdir($UserResumePath,0777,true);
        }
        if (!is_dir($UserVideoPath)) { 
            mkdir($UserVideoPath,0777,true);
        }
    }

}
