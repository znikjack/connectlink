<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Image_model extends CI_Model {

    public function insert_image($data) {
        if(!empty($data['id'])){
            $this->db->where('id', $data['id']);
            $this->db->update('images', $data);
            return $data['id'];
        }else{
            $this->db->insert('images',$data);
            return $this->db->insert_id();
        }
    }

    public function get_imageslist() {

        $this->db->select('i.*,c.name as category_name');
        $this->db->from('images i');
        $this->db->join('categorys c','c.id = i.category_id');
        $this->db->order_by('i.id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_imageslist_by_ID($category_id = false, $limit, $offset, $onlycount = '') {

        $this->db->select('i.*,c.name as category_name');
        $this->db->from('images i');
        $this->db->join('categorys c','c.id = i.category_id');
        $this->db->order_by('i.id', 'DESC');
        if ($onlycount != 'onlycount') {
            $this->db->limit($limit, $offset);
        }
        $this->db->where('c.id', $category_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_images_byID($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        $this->db->from('images');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }

    public function delete($id) {

        $this->db->where('id', $id);
        $this->db->delete('images');
    }

    /**
     * Query of update data 
     *
     * @param   $table_name, $field, $val, $data
     * @return  Not return
     */
    public function update($table_name, $field, $val, $data = array()) {

        $this->db->where($field, $val);
        $this->db->update($table_name, $data);
    }
    
    public function get_Category(){
        $this->db->select('*');
        $result = $this->db->get('categorys');
        return $result->result();
    }

}
