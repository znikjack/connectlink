<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SocialHeader_model extends CI_Model {

    public function GetAllCategorys($table, $field1, $sorting) {
        $this->db->select('*');
        $this->db->order_by($field1, $sorting);
        $result = $this->db->get($table);
        return $result->result();
    }

    public function GetCategoryById($table, $field1, $val1) {
        $this->db->select('*');
        $this->db->where($field1, $val1);
        $result = $this->db->get($table);
        return $result->row();
    }

    public function delete($table, $field1, $val1) {
        $this->db->where($field1, $val1);
        $this->db->delete($table);
    }

    public function save($table, $data) {
        if ($data['id']) {
            $this->db->where('id', $data['id']);
            $this->db->update($table, $data);
            return $data['id'];
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }
    
    public function GetAllSocial($table, $field1, $sorting) {
        $this->db->select('sl.*,sh.Social_Header_Name');
        $this->db->from('social_list sl');
        $this->db->join('social_header sh', 'sh.id = sl.Social_Header_ID');
        $this->db->order_by('sl.id', $sorting);
        $result = $this->db->get();
        return $result->result();
    }

}
