<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apiservice_model extends CI_Model {

    public function get_check_login($table_name, $field1, $val1, $field2, $val2) {
        if ($field1) {
            $this->db->where($field1, $val1);
        }
        if ($field2) {
            $this->db->where($field2, sha1($val2));
        }
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->row();
    }

    public function AlreadyUserExitsCheck($table_name, $field1, $val1) {
        $this->db->where($field1, $val1);
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->row();
    }
    public function GetUserProfileDataByID($table_name, $field1, $val1) {
        $this->db->where($field1, $val1);
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function update_device_token($table_name, $field, $val,$data=array()){
    	$this->db->where($field, $val);
    	$this->db->update($table_name, $data);
    }
    public function UpdateUserProfileDataByID($table_name,$data,$field,$id) {
        $this->db->where($field, $id);
    	$this->db->update($table_name, $data);
    }
    public function InsertData($table_name, $data=array()) {
    	$this->db->insert($table_name, $data);
    	return $this->db->insert_id(); 
        
    }
    public function check_otp_pass($table, $field1, $val1, $field2, $val2){

        $this->db->where($field1, $val1);
        $this->db->where($field2, $val2);
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->row();    
    }
    
    public function reset_password($email,$data) {
        $this->db->where('EmailAddress', $email);
    	$this->db->update('users', $data);
        return true;
    }

}
