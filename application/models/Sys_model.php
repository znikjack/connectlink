<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sys_model extends CI_Model {

    public function check_user($table, $field1, $val1, $field2, $val2) {

        $this->db->where($field1, $val1);
        $this->db->where($field2, $val2);
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function GetSelectData($table,$field1,$sorting) {
        $this->db->select('*');
        $this->db->order_by($field1,$sorting);
        $result = $this->db->get($table);
        return $result->result();
    }

}
