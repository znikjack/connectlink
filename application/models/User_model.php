<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function check_user($table, $field1, $val1, $field2, $val2) {

        $this->db->where($field1, $val1);
        $this->db->where($field2, $val2);
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->row();
    }

    public function GetSelectData($table, $field1, $sorting) {
        $this->db->select('*');
        $this->db->order_by($field1, $sorting);
        $result = $this->db->get($table);
        return $result->result();
    }

    public function GetUserInfo($table, $field1, $val1) {
        $this->db->select('*');
        $this->db->where($field1, $val1);
        $result = $this->db->get($table);
        return $result->row();
    }

    public function delete($table, $field1, $val1) {
        $this->db->where($field1, $val1);
        $this->db->delete($table);
    }

    public function save($table, $data) {
        if ($data['id']) {
            $this->db->where('id', $data['id']);
            $this->db->update($table, $data);
            return $data['id'];
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }
    function check_emailID($email){
        $this->db->select('EmailAddress');
        $this->db->where('EmailAddress',$email);
        $result = $this->db->get('Users');
        $row = $result->num_rows();
        if( $row > 0 ){
            echo 'false';
        } else {
            echo 'true';
        }
    }

}
