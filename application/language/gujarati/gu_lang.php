<?php
$lang['HOME'] = 'હોમ';
$lang['About Sutrapada'] = 'સૂત્રપડા વિશે';
$lang['About Nagarpalika'] = 'નાગરપાલિકા વિશે';
$lang['Information Rights'] = 'માહિતી અધિકાર';
$lang['Citizen Rights'] = 'નાગરિક અધિકાર';
$lang['Form Download'] = 'ફોર્મ ડાઉનલોડ';
$lang['Important Contacts'] = 'મહત્વપૂર્ણ સંપર્કો';
$lang['News'] = 'ન્યૂઝ';
$lang['Programs'] = 'પ્રોગ્રામ';
$lang['Complain'] = 'ફરિયાદ';
$lang['Contact'] = 'સંપર્ક';

/* End of file en_lang.php */
/* Location: ./application/language/english/en_lang.php */