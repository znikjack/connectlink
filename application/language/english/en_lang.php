<?php
$lang['HOME'] = 'Home';
$lang['About Sutrapada'] = 'About Sutrapada';
$lang['About Nagarpalika'] = 'About Nagarpalika';
$lang['Information Rights'] = 'Information Rights';
$lang['Citizen Rights'] = 'Citizen Rights';
$lang['Form Download'] = 'Form Download';
$lang['Important Contacts'] = 'Important Contacts';
$lang['News'] = 'News';
$lang['Programs'] = 'Programs';
$lang['Complain'] = 'Complain';
$lang['Contact'] = 'Contact';

/* End of file en_lang.php */
/* Location: ./application/language/english/en_lang.php */