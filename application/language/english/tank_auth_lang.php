<?php

// Errors
$lang['auth_incorrect_password'] = 'Incorrect password';
$lang['auth_incorrect_login'] = 'Incorrect login';
$lang['auth_incorrect_email_or_username'] = 'Login or email doesn\'t exist';
$lang['auth_email_in_use'] = 'Email is already used by another user. Please choose another email.';
$lang['auth_username_in_use'] = 'Username already exists. Please choose another username.';
$lang['auth_current_email'] = 'This is your current email';
$lang['auth_incorrect_captcha'] = 'Your confirmation code does not match the one in the image.';
$lang['auth_captcha_expired'] = 'Your confirmation code has expired. Please try again.';

// Notifications
$lang['auth_message_logged_out'] = 'You have been successfully logged out.';
$lang['auth_message_registration_disabled'] = 'Registration is disabled.';
$lang['auth_message_registration_completed_1'] = 'You have successfully registered. Check your email address to activate your account.';
$lang['auth_message_registration_completed_2'] = 'You have successfully registered.';
$lang['auth_message_activation_email_sent'] = 'A new activation email has been sent from GoGoHill. Follow the instructions in the email to activate your account.';
$lang['auth_message_activation_completed'] = 'Your account has been successfully activated.';
$lang['auth_message_activation_failed'] = 'The activation code you entered is incorrect or expired.';
$lang['auth_message_password_changed'] = 'Your password has been successfully changed.';
$lang['auth_message_new_password_sent'] = 'An email with instructions for creating a new password has been sent to you.';
$lang['auth_message_new_password_activated'] = 'You have successfully reset your password';
$lang['auth_message_new_password_failed'] = 'Your activation key is incorrect or expired. Please check your email again and follow the instructions.';
$lang['auth_message_new_email_sent'] = 'A confirmation email has been sent from GoGoHill. Follow the instructions in the email to complete this change of email address.';
$lang['auth_message_new_email_activated'] = 'You have successfully changed your email';
$lang['auth_message_new_email_failed'] = 'Your activation key is incorrect or expired. Please check your email again and follow the instructions.';
$lang['auth_message_banned'] = 'You are banned.';
$lang['auth_message_unregistered'] = 'Your account has been deleted...';

// Email subjects
$lang['auth_subject_welcome'] = 'Welcome to GoGoHill!';
$lang['auth_subject_activate'] = 'Welcome to GoGoHill!';
$lang['auth_subject_forgot_password'] = 'Forgot your password on GoGoHill?';
$lang['auth_subject_reset_password'] = 'Your new password on GoGoHill';
$lang['auth_subject_change_email'] = 'Your new email address on GoGoHill';
$lang['auth_subject_new_admin_created'] = 'New admin created on %s';
$lang['auth_subject_approval'] = 'Your request for adding a restaurant on %s is approved';
$lang['auth_subject_forgot_password_admin'] = 'Reset password on %s';


$lang['auth_all_fields_madatory'] = 'Please enter all fields.';
$lang['auth_invalid_email'] = 'Incorrect email ID';
$lang['auth_error_occurred'] = 'Error Occurred, Try again.';
$lang['auth_verify_email']	= 'Please verify your email.';
$lang['auth_subject_approval']	= 'Your request for %s is approved by Gogohill.';
$lang['auth_subject_add_restaurant']	= 'Your restaurant %s has been added.';
$lang['auth_subject_email_notification'] = 'You have a Notification from Gogohill';

$lang['auth_password_not_match'] = 'Your new password and confirm passord does not match';
$lang['auth_admin_reset_link_invalid'] = 'The link to reset passord is invalid, Try again to reset your password';
$lang['auth_admin_reset_pwd_fail'] = 'Unable to reset your password, Try again';
$lang['auth_changed_password'] = 'Your password Changed Successfully';

$lang['restaurant_add_notify_admin'] = "A new restaurant \"<b>%s</b>\" has been successfully added.";
$lang['auth_subject_publish_resturant_notify_admin'] = 'A new restaurant is published on %s';

/* End of file tank_auth_lang.php */
/* Location: ./application/language/english/tank_auth_lang.php */