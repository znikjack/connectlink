jQuery(document).ready(function(){
        var base_url = $('.base_url').val();
         jQuery("#user_register").validate({
            rules: {
                FirstName:
                {
                    required: true,lettersonly: true
                },
                LastName:
                {
                    required: true,lettersonly: true
                },
                EmailAddress:
                {
                    required: true,
                    EmailAddress: true,
                    remote: {
                        url: base_url+'/user/checkEmail',
                        type: "post"
                    }
                    
                },
                MobileNumber:
                {
                    required: true,/*digits: true*/
                    maxlength: 14,
                },
                gender:
                {
                    required: true,
                },
                address:
                {
                    required: true,
                }, 
                Password:
                {
                    required: true,
                }, 
                bod:
                {
                    required: true,
                },
                Bio:
                {
                    required: true,
                },
                ProfileURL:
                {
                    required: true,
                },
                BackgroundURL:
                {
                    required: true,
                },
               
            },
            messages: {
                FirstName:
                {
                    required: "Please enter First Name !"
                },
                LastName:
                {                    
                   
                    required: "Please enter Last Name !"
                },
                EmailAddress:
                {
                       required: "Please enter Email Address !",
                       remote: "Email already in use!"
                    
                },
                MobileNumber:
                {
                    required: "Please enter Phone Number !"
                   
                }, 
                gender:
                {
                    required: "Please enter gender !"
                   
                },
                address:
                {
                    required: "Please enter Address!"
                    
                },
                Password:
                {
                    required: "Please enter Password!"
                    
                },
                bod:
                {
                    required: "Please select date of birth!"
                },               
                Bio:
                {
                    required: "Please select Bio!"
                },               
                ProfileURL:
                {
                    required: "Please select User Profile!"
                },               
                BackgroundURL:
                {
                    required: "Please select Backround Image!"
                },               
            }  
    }); 
           
});


//$.validator.addMethod("indianDate", function (value, element) {return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);}, "Please enter a date in the format dd-mm-yyyy");
$.validator.addMethod("lettersonly", function(value, element) {return this.optional(element) || /^[a-z," "]+$/i.test(value);}, "Letters only please");